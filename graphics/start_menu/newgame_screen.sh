#!/bin/bash

#
#	Скрин начала новой игры
#

clear

saving_data ()
{
	touch $save/save.pd

	if test -f $save/save.pd; then
		echo '${name} ${ages}' >> $save/save.pd

		return true
	else
		echo 'ФАЙЛА save.pd НЕ СУЩЕСТВУЕТ!'

		return false
	fi
}

DIALOG=${DIALOG=dialog}

tempfile=`mktemp 2>/dev/null` || tempfile=/tmp/test$$
trap "rm -f $tempfile" 0 1 2 5 15

tempfile2=`mktemp 2>/dev/null` || tempfile2=/tmp/test$$
trap "rm -f $tempfile2" 0 1 2 5 15

tempfile3=`mktemp 2>/dev/null` || tempfile3=/tmp/test$$
trap "rm -f $tempfile3" 0 1 2 5 15


$DIALOG --title "Новая игра"
		--inputbox "Введите ваше имя" 5 40 2> $tempfile

$DIALOG --title "Новая игра"
		--inputbox "Введите ваш возраст" 5 40 2> $tempfile2

$DIALOG --title "Новая игра"
		--inputbox "Выберите место сохранение (все сохранаяется в файл save)" 5 50 2> $tempfile3

#----- Введеные данные в переменных -----#
	
	name='cat $tempfile'
	ages='cat $tempfile2'
	save='cat $tempfile3'

#----- /Введеные данные в переменных -----# 

case saving_data in

	true)
		echo "Продолжаем играть"
	;;
	false)
		clear

		echo "Игра не может запуститься без сохранения!"
		exit
	;;

esac