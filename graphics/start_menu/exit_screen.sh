#!/bin/bash

#
#	Скрин выхода
#

DIALOG=${DIALOG=dialog}

back_to_welcome ()
{
	clear

	graphics/welcome_screen.sh
}

exit_fnc ()
{
	clear

	exit
}

$DIALOG --title "Выход из игры" --yesno "Вы точно хотите выйти?" 5 40

case $? in

	1) # Выбрано "Yes"
		back_to_welcome
	;;
	0) # Выбрано "No"
		exit_fnc
	;;
	255) # Нажата клавиша ESC
		back_to_welcome
	;;

esac