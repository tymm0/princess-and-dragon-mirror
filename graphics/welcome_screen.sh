#!/bin/sh

# 
#   Главное меню игры
#

DIALOG=${DIALOG=dialog}
tempfile=`mktemp 2>/dev/null` || tempfile=/tmp/test$$
trap "rm -f $tempfile" 0 1 2 5 15

$DIALOG --clear --title "Princess&Dragon" \
        --menu "Start Menu" 20 51 4 \
        "Новая игра" "начать новую игру" \
        "Сохранённая игра" "загрузить сохранение" \
        "Авторы" "авторы этой игры" \
        "Выйти из игры" "выход" 2> $tempfile

retval=$?

choice=`cat $tempfile` 

case $retval in # Выбор действия

  0) # Был выбран какой-то путь

    case $choice in # Проверяем пункт выбранный пользователем

      "Новая игра") # Грузим скрин новой игры
        source graphics/start_menu/newagme_screen.sh
      ;;
      "Сохранённая игра") # Грузим скрин сохранения
      ;;
      "Авторы") # Грузим скрин с авторами
      ;;
      "Выйти из игры") # Грузим скрин выбора
        source graphics/start_menu/exit_screen.sh
      ;;

    esac

  ;;
  1) # Отказ от ввода
  ;;
  255) # Была нажата клавиша ESC
  ;;

esac