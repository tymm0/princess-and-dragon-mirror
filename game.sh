#!/bin/bash

#
#	Игра Princes&Dragon
#
#		Написана tymm0
#		Распространяется свободно
#
#	HackWork (c) 2016
#

# Функция входа в игру

auto_chmod ()
{
	#graphics
		#cut_scenes
			chmod +x graphics/first_scene.sh
		#game_menu
			# ---------------- #
		#start_menu
			chmod +x graphics/start_menu/exit_screen.sh
			chmod +x graphics/start_menu/welcome_screen.sh
	#bin
		#map_upload
			# ---------------- #
		chmod +x bin/update_map.sh
	#ai
		# ---------------- #
	#maps
}

welcome ()
{
	clear

	source graphics/welcome_screen.sh
}

# Функция выхода из игры
dewelcome ()
{
	clear

	echo "Был совершен выход из игры"
	exit
}

clear # Очищаем экран
echo ""
echo ""
echo "PRINCESS AND DRAGON"
echo ""
echo "			Author: tymm0"
echo ""

echo "Продолжить запускать игру? [Y|N] \c"

# Узнаем выбор
read item
case "$item" in

	y|Y) # Выбор yes
		auto_chmod

		welcome
	;;
	n|N) # Выбор no
		dewelcome
	;;
	*) # Выбор если выбрано не пойми что
		auto_chmod

		welcome
	;;
esac